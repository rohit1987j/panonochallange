package com.panono.service;

import com.panono.domain.StatisticsResponse;

public interface StatisticsService {
    public StatisticsResponse getStatistics(long timestampWhenRequestReceived);
}
