package com.panono.service.impl;

import com.panono.domain.UploadRequest;
import com.panono.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

@Service
public class UploadServiceImpl implements UploadService {
    @Autowired
    ConcurrentHashMap<Long, Integer> uploadCountRepository;

    public ConcurrentHashMap<Long, Integer> getUploadCountRepository() {
        return uploadCountRepository;
    }

    @Override
    public void upload(UploadRequest uploadRequest, long timeWhenRequestReceived) {
        synchronized (getUploadCountRepository()) {
            Integer count = getUploadCountRepository().get(timeWhenRequestReceived);
            getUploadCountRepository().put(timeWhenRequestReceived, count == null ?
                    uploadRequest.getCount() : uploadRequest.getCount() + count);
        }
    }
}