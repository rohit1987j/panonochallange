package com.panono.service.impl;

import com.panono.domain.StatisticsResponse;
import com.panono.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    ConcurrentHashMap<Long, Integer> uploadRepository;

    public ConcurrentHashMap<Long, Integer> getUploadRepository() {
        return uploadRepository;
    }

    public StatisticsResponse getStatistics(long timestampWhenRequestReceived) {
        StatisticsResponse response = new StatisticsResponse();
        synchronized (getUploadRepository()) {
            int sum = getLast60SecondsRecordsStream(timestampWhenRequestReceived).sum();
            OptionalDouble average = getLast60SecondsRecordsStream(timestampWhenRequestReceived).average();
            long count = getLast60SecondsRecordsStream(timestampWhenRequestReceived).count();
            OptionalInt min = getLast60SecondsRecordsStream(timestampWhenRequestReceived).min();
            OptionalInt max = getLast60SecondsRecordsStream(timestampWhenRequestReceived).max();
            response.setSum(sum);
            response.setAvg(average.orElse(0));
            response.setCount(count);
            response.setMax(max.orElse(0));
            response.setMin(min.orElse(0));
        }
        return response;
    }

    private IntStream getLast60SecondsRecordsStream(long timestampWhenRequestReceived) {
        return getUploadRepository().keySet().parallelStream().filter(timeStamp ->
                (timestampWhenRequestReceived - timeStamp) <= 60).mapToInt(
                e -> getUploadRepository().get(e));
    }
}