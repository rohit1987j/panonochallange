package com.panono.service;

import com.panono.domain.UploadRequest;

public interface UploadService {
    public void upload(UploadRequest uploadRequest, long timeWhenRequestReceived);
}
