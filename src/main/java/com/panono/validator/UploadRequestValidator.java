package com.panono.validator;

import com.panono.domain.UploadRequest;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

import static com.panono.utilities.Constants.VALIDATION_ERRRORS.COUNT_CANNOT_BE_LESS_THEN_ZERO;
import static com.panono.utilities.Constants.VALIDATION_ERRRORS.TIMESTAMP_CANNOT_BE_NULL;

public class UploadRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UploadRequest.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UploadRequest uploadRequest = (UploadRequest) target;
        if (Objects.isNull(uploadRequest.getCount()) || uploadRequest.getCount() < 1) {
            errors.reject(COUNT_CANNOT_BE_LESS_THEN_ZERO);
        }
        if (Objects.isNull(uploadRequest.getTimestamp())) {
            errors.reject(TIMESTAMP_CANNOT_BE_NULL);
        }
    }
}
