package com.panono.utilities;

public class Constants {
    public static final long TIME_DIFFERENCE = 60;

    public static final class API_URL {
        public static final String UPLOAD = "/upload";
        public static final String STATISTICS = "/statistics";
    }

    public static final class VALIDATION_ERRRORS {
        public static final String COUNT_CANNOT_BE_LESS_THEN_ZERO = "430";
        public static final String TIMESTAMP_CANNOT_BE_NULL = "431";
        public static final String TIMESTAMP_CANNOT_BE_OLDER_THEN_60_SECONDS = "432";
    }
}
