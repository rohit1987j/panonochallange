package com.panono.controller;

import com.panono.domain.StatisticsResponse;
import com.panono.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

import static com.panono.utilities.Constants.API_URL.STATISTICS;

@RestController
public class StatisticsController {

    @Autowired
    StatisticsService statisticsService;

    public StatisticsService getStatisticsService() {
        return statisticsService;
    }

    @GetMapping(STATISTICS)
    public ResponseEntity<StatisticsResponse> getStatistics() {
        long timeWhenRequestReceived = Instant.now().getEpochSecond();
        return new ResponseEntity(getStatisticsService().getStatistics(timeWhenRequestReceived), HttpStatus.OK);
    }
}
