package com.panono.controller;

import com.panono.domain.UploadRequest;
import com.panono.service.UploadService;
import com.panono.validator.UploadRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;

import static com.panono.utilities.Constants.API_URL.UPLOAD;
import static com.panono.utilities.Constants.TIME_DIFFERENCE;
import static com.panono.utilities.Constants.VALIDATION_ERRRORS.TIMESTAMP_CANNOT_BE_OLDER_THEN_60_SECONDS;

@RestController
public class UploadController {
    @Autowired
    UploadService uploadService;

    public UploadService getUploadService() {
        return uploadService;
    }

    @InitBinder
    protected void uploadRequestDataBinding(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(new UploadRequestValidator());
    }

    @PostMapping(name = UPLOAD)
    public ResponseEntity upload(@Valid @RequestBody UploadRequest uploadRequest, Errors errors) {
        long timeWhenRequestReceived = Instant.now().getEpochSecond();
        if (errors.hasErrors() || ((Instant.now().getEpochSecond()) - uploadRequest.getTimestamp()) > TIME_DIFFERENCE) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        getUploadService().upload(uploadRequest, timeWhenRequestReceived);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
