package com.panono.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class ApplicationConfig {

    @Bean
    @Scope("singleton")
    public ConcurrentHashMap<Long, Integer> uploadCountRepository() {
        return new ConcurrentHashMap<Long, Integer>();
    }
}