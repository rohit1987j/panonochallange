package com.panono.integration.controller;

import com.panono.domain.StatisticsResponse;
import com.panono.domain.UploadRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.panono.utilities.Constants.API_URL.STATISTICS;
import static com.panono.utilities.Constants.API_URL.UPLOAD;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void uploadShouldReturnStatus204TimestampIsOlderThan60Seconds() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(1);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond() - 61);
        ResponseEntity responseEntity = restTemplate.postForEntity(
                UPLOAD, uploadRequest, null);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void uploadShouldReturnStatus204WhenCountIsLessThen1() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(0);
        ResponseEntity responseEntity = restTemplate.postForEntity(
                UPLOAD, uploadRequest, null);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void uploadShouldReturnStatus201WhenCountIsGreaterThen0AndTimestampIsLessThen60Seconds() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(1);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond());
        ResponseEntity responseEntity = restTemplate.postForEntity(
                UPLOAD, uploadRequest, null);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }


    @Test
    public void stressTest() throws InterruptedException {
        int counter = 1;
        while (counter <= 5) {
            final long uploadTime = Instant.now().getEpochSecond();
            uploadUsingExecutorService(uploadTime);
            counter++;
        }
        Thread.sleep(20000);
        StatisticsResponse statisticsResponse = restTemplate.getForObject(
                STATISTICS, StatisticsResponse.class);
        assertEquals(5* 100, statisticsResponse.getSum());
    }

    public void uploadUsingExecutorService(long uploadTime) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.execute(() -> {
            int counter = 1;
            while (counter <= 100) {
                UploadRequest uploadRequest = new UploadRequest();
                uploadRequest.setCount(1);
                uploadRequest.setTimestamp(uploadTime);
                ResponseEntity responseEntity = restTemplate.postForEntity(
                        UPLOAD, uploadRequest, null);
                counter++;
            }
        });
        Thread.sleep(1000);
        executorService.shutdown();
    }
}
