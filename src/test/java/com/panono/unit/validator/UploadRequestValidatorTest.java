package com.panono.unit.validator;

import com.panono.domain.UploadRequest;
import com.panono.validator.UploadRequestValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import java.time.Instant;
import java.util.Arrays;

import static com.panono.utilities.Constants.VALIDATION_ERRRORS.COUNT_CANNOT_BE_LESS_THEN_ZERO;
import static com.panono.utilities.Constants.VALIDATION_ERRRORS.TIMESTAMP_CANNOT_BE_NULL;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

public class UploadRequestValidatorTest {
    UploadRequestValidator uploadRequestValidator;
    UploadRequest uploadRequest;
    Errors errors;

    @Before
    public void init() {
        uploadRequestValidator = spy(UploadRequestValidator.class);
        uploadRequest = new UploadRequest();
    }

    @Test
    public void validateShouldPutErrorsInErrorObjectWhenCountIsNull() {
        Errors errors = new BeanPropertyBindingResult(uploadRequest, "uploadRequest");
        uploadRequestValidator.validate(uploadRequest, errors);
        assertTrue(errors.getAllErrors().parallelStream().flatMap(e -> Arrays.asList(e.getCodes()).parallelStream()).filter(
                e -> e.equals(COUNT_CANNOT_BE_LESS_THEN_ZERO)).findFirst().isPresent());
    }

    @Test
    public void validateShouldPutErrorsInErrorObjectWhenTimestampIsNull() {
        Errors errors = new BeanPropertyBindingResult(uploadRequest, "uploadRequest");
        uploadRequestValidator.validate(uploadRequest, errors);
        assertTrue(errors.getAllErrors().parallelStream().flatMap(e -> Arrays.asList(e.getCodes()).parallelStream()).filter(
                e -> e.equals(TIMESTAMP_CANNOT_BE_NULL)).findFirst().isPresent());
    }

    @Test
    public void validateShouldNotPutErrorsInErrorObjectWhenUploadRequestIsFine() {
        uploadRequest.setCount(1);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond());
        Errors errors = new BeanPropertyBindingResult(uploadRequest, "uploadRequest");
        uploadRequestValidator.validate(uploadRequest, errors);
        assertTrue(errors.getAllErrors().size() == 0);
    }
}
