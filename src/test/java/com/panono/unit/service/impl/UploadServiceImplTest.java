package com.panono.unit.service.impl;

import com.panono.domain.UploadRequest;
import com.panono.service.impl.UploadServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class UploadServiceImplTest {
    private UploadServiceImpl uploadService;
    private ConcurrentHashMap<Long, Integer> uploadCountRepository;
    private ExecutorService executorService;

    @Before
    public void init() {
        executorService = Executors.newFixedThreadPool(10);
        uploadService = Mockito.spy(UploadServiceImpl.class);
        uploadCountRepository = new ConcurrentHashMap<Long, Integer>();
        when(uploadService.getUploadCountRepository()).thenReturn(uploadCountRepository);
    }

    @Test
    public void multipleUploadAtSameTime() throws InterruptedException {
        final long timestampWhenRequestReceived = Instant.now().getEpochSecond();
        uploadUsingExecutorService(timestampWhenRequestReceived);
        Thread.sleep(1000);
        assertEquals(1, uploadCountRepository.size());
        assertEquals(10000, uploadCountRepository.get(timestampWhenRequestReceived).intValue());
    }

    @Test
    public void multipleUploadAtDifferentTime() throws InterruptedException {
        int counter = 1;
        while (counter <= 5) {
            final long timestampWhenRequestReceived = Instant.now().getEpochSecond();
            uploadUsingExecutorService(timestampWhenRequestReceived);
            Thread.sleep(1000);
            counter++;
        }
        assertEquals(5, uploadCountRepository.size());
        assertEquals(5 * 10000,
                uploadCountRepository.values().parallelStream().mapToInt(e -> e).sum());
    }

    public void uploadUsingExecutorService(long timeWhenRequestReceived) {
        executorService.execute(() -> {
            int counter = 1;
            while (counter <= 10000) {
                UploadRequest uploadRequest = new UploadRequest();
                uploadRequest.setTimestamp(timeWhenRequestReceived);
                uploadRequest.setCount(1);
                uploadService.upload(uploadRequest, timeWhenRequestReceived);
                counter++;
            }
        });
    }
}