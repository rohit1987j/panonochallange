package com.panono.unit.service.impl;

import com.panono.domain.StatisticsResponse;
import com.panono.service.impl.StatisticsServiceImpl;
import org.junit.Test;

import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class StatisticsServiceImplTest {
    @Test
    public void calculateStatisticsServiceShouldReturnStatisticsResponseWithZeroValuesWhenNoUploadInLast60Seconds() {
        StatisticsServiceImpl statisticsService = spy(StatisticsServiceImpl.class);
        when(statisticsService.getUploadRepository()).thenReturn(
                new ConcurrentHashMap<Long, Integer>());
        long timeStampWhenRequestReceived = Instant.now().getEpochSecond();
        StatisticsResponse statisticsResponse = statisticsService.getStatistics(timeStampWhenRequestReceived);
        assertEquals(0, statisticsResponse.getMax());
        assertEquals(0, statisticsResponse.getMin());
        assertEquals(0, statisticsResponse.getCount());
        assertEquals(0, statisticsResponse.getSum());
    }
}
