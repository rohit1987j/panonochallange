package com.panono.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.panono.controller.StatisticsController;
import com.panono.domain.StatisticsResponse;
import com.panono.service.impl.StatisticsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.panono.utilities.Constants.API_URL.STATISTICS;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private StatisticsServiceImpl statisticsService;

    @Test
    public void getStatistics() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                STATISTICS).accept(
                MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }
}
