package com.panono.unit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.panono.controller.UploadController;
import com.panono.domain.UploadRequest;
import com.panono.service.impl.UploadServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;

import static com.panono.utilities.Constants.API_URL.UPLOAD;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(UploadController.class)
public class UploadControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    UploadServiceImpl uploadService;

    @Test
    public void uploadShouldReturn204ResponseStatusWhenCountInUploadRequestIsLessThen1() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(0);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond());
        ObjectMapper mapper = new ObjectMapper();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                UPLOAD).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(uploadRequest));
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus());
    }

    @Test
    public void uploadShouldReturn204ResponseStatusWhenTimestampIsOlderThen60Seconds() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(1);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond() - 61);
        ObjectMapper mapper = new ObjectMapper();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                UPLOAD).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(uploadRequest));
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        assertEquals(HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus());
    }

    @Test
    public void uploadShouldReturn201ResponseStatusWhenTimestampIsLessOlderThen60SecondsAndCountGreaterThen0() throws Exception {
        UploadRequest uploadRequest = new UploadRequest();
        uploadRequest.setCount(1);
        uploadRequest.setTimestamp(Instant.now().getEpochSecond());
        ObjectMapper mapper = new ObjectMapper();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                UPLOAD).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(uploadRequest));
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());
    }
}
