#To run this project run following commands in terminal:  
* git clone rohit1987j@bitbucket.org:rohit1987j/panonochallange.git  
* cd ./panonochallange  
* gradle build  
* gradle bootRun  
  
#Repository Url: https://bitbucket.org/rohit1987j/panonochallange

#upload - post : http://localhost:8080/upload
#statistics - get : http://localhost:8080/statistics
  
#Remaining details mentioned in the pdf provided.  